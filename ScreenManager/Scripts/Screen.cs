﻿using UnityEngine;
using UnityEngine.Events;

namespace Lambda.ScreenManager
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(CanvasGroup))]
    public class Screen : MonoBehaviour
    {
        public Animator Animator;
        public UnityEvent OnOpen;
        public UnityEvent OnClose;
        public bool KeepAlive;
        public float LiveFor = 2;
        public object Data;

        [HideInInspector]
        public ScreenManager ScreenManager;

        public void IsOpen()
        {
            OnOpen.Invoke();
        }

        public void IsClosed()
        {
            OnClose.Invoke();
        }

        public void Open()
        {
            ScreenManager.OpenScreen(this);
        }

        public void SendData(object data)
        {
            Data = data;
        }
    }
}
