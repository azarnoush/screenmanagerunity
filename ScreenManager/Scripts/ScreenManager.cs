﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Lambda.ScreenManager
{
    public class ScreenManager : MonoBehaviour
    {
        public Screen InitialScreen;
        public Screen WorkingScreen;
        public bool ResetOnClose;
        public bool UseAnimator;
        [HideInInspector]
        public Screen CurrentScreen { get { return _currentScreen; } }
        private Screen _currentScreen;
        private Screen _lastScreen;
        private Action _lastScreenCancelCloseAction;

        private const string OPEN_TRIGGER_KEY = "Open";
        private const string CLOSE_TRIGGER_KEY = "Close";
        private const string ACTIVATE_TRIGGER_KEY = "Activate";
        private const string DEACTIVE_TRIGGER_KEY = "Deactive";
        private bool _acceptBackButton;
        public void OpenScreen(Screen screen, object data)
        {
            if (screen == null || _currentScreen == screen)
                return;
            
            if (_lastScreen == screen)
                if(_lastScreenCancelCloseAction!=null)
                    _lastScreenCancelCloseAction();

            if (_currentScreen != null)
                CloseScreen(_currentScreen);

            ShowScreen(screen, data);
        }
        public void OpenScreen(Screen screen)
        {
            OpenScreen(screen, null);
        }

        public void DeactivateCurrent()
        {
            if(_currentScreen==null) return;
            _currentScreen.Animator.SetTrigger(DEACTIVE_TRIGGER_KEY);
        }

        public void ActivateCurrent()
        {
            if(_currentScreen==null) return;
            _currentScreen.Animator.SetTrigger(ACTIVATE_TRIGGER_KEY);
        }
        public void OnEnable()
        {
            var screens = GetScreensAsList();
            foreach (var screen in screens)
            {
                screen.gameObject.SetActive(false);

                if (!UseAnimator)
                {
                    screen.gameObject.SetActive(false);
                    screen.GetComponent<Animator>().enabled = false;
                    screen.GetComponent<CanvasGroup>().alpha = 1;
                }
                screen.ScreenManager = this;
            }
            if (InitialScreen == null)
                return;

            ShowScreen(InitialScreen);
        }

        private List<Screen> GetScreensAsList()
        {
            var screens = new List<Screen>();
            foreach (Transform child in transform)
            {
                var screenManager = child.GetComponent<Screen>();
                if (screenManager != null)
                    screens.Add(screenManager);
            }
            return screens;
        }

        private void CloseScreen(Screen screen)
        {
            if (screen == null)
                return;
            _lastScreen = screen;
            var groupCanvas = screen.GetComponent<CanvasGroup>();
            if (groupCanvas != null)
                groupCanvas.blocksRaycasts = false;
            var screenManager = screen.GetComponent<ScreenManager>();
            if (screenManager != null)
            {
                //this is nested screen manager
                if (screenManager.ResetOnClose)
                    screenManager.Reset();
            }
            if (UseAnimator)
                screen.Animator.SetTrigger(CLOSE_TRIGGER_KEY);
            else if (!screen.KeepAlive)
                RunWithDelay(() => screen.gameObject.SetActive(false), screen.LiveFor, out _lastScreenCancelCloseAction);
            screen.IsClosed();
        }

        private void Reset()
        {
            if(_currentScreen != InitialScreen)
                CloseScreen(_currentScreen);
            ShowScreen(InitialScreen);
        }

        private void ShowScreen(Screen screen, object data = null)
        {
            if (screen == null) return;
            screen.gameObject.SetActive(true);
            var groupCanvas = screen.GetComponent<CanvasGroup>();
            if (groupCanvas != null)
                groupCanvas.blocksRaycasts = true;
            if (UseAnimator)
                screen.Animator.SetTrigger(OPEN_TRIGGER_KEY);
            _currentScreen = screen;
            _currentScreen.transform.SetAsLastSibling();
            _currentScreen.SendData(data);
            _currentScreen.IsOpen();
        }

        private void RunWithDelay(Action action, float delay, out Action cancelAction)
        {
            var coroutine = StartCoroutine(RunWithDelayEnumerator(action, delay));
            cancelAction = () => StopCoroutine(coroutine);
        }

        [UsedImplicitly]
        private void RunWithDelay(Action action, float delay)
        {
            StartCoroutine(RunWithDelayEnumerator(action, delay));
        }

        private IEnumerator RunWithDelayEnumerator(Action action, float delay)
        {
            yield return new WaitForSeconds(delay);
            if (action != null)
                action();
        }
    }
}
