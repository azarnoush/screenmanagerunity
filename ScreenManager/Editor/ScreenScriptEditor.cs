﻿using UnityEngine;
using UnityEditor;

namespace Lambda.ScreenManager.EditorScripts
{
    [CustomEditor(typeof(Screen))]
    public class ScreenScriptEditor : Editor
    {
        private Screen _myScreen;

        private SerializedProperty _animator;
        private SerializedProperty _onCloseEvent;
        private SerializedProperty _onOpenEvent;

        protected void OnEnable()
        {
            _animator = serializedObject.FindProperty("Animator");
            _onCloseEvent = serializedObject.FindProperty("OnClose");
            _onOpenEvent = serializedObject.FindProperty("OnOpen");

            _myScreen = target as Screen;

            if (_myScreen == null)
                return;

            if (_myScreen.Animator == null)
                _myScreen.Animator = GetMyAnimator();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(_animator, new GUIContent("Animator"));
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(_onOpenEvent, new GUIContent("On Open"));
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(_onCloseEvent, new GUIContent("On Close"));

            serializedObject.ApplyModifiedProperties(); 
        }

        private Animator GetMyAnimator()
        {
            return _myScreen.GetComponent<Animator>();
        }
    }
}
