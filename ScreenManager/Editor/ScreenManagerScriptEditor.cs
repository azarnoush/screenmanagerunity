﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

namespace Lambda.ScreenManager.EditorScripts
{
    [CustomEditor(typeof(ScreenManager))]
    public class ScreenManagerScriptEditor : Editor
    {
        private ScreenManager _myScreenManager;
        private int _initialScreenIndex;
        private int _workingScreenIndex;
        private SerializedProperty _initialScreen;
        private SerializedProperty _resetOnClose;
        private SerializedProperty _useAnimator;

        protected void OnEnable()
        {

            _initialScreen = serializedObject.FindProperty("InitialScreen");
            _resetOnClose = serializedObject.FindProperty("ResetOnClose");
            _useAnimator = serializedObject.FindProperty("UseAnimator");
            _myScreenManager = target as ScreenManager;
            ChangeWorkingScreen(GetCurrentWorkingScreenIndex());

        }

        private int GetCurrentInitialScreenIndex()
        {
            var screens = GetScreensAsList();
            return _myScreenManager.InitialScreen == null
                ? 0
                : screens.TakeWhile(screen => screen != _myScreenManager.InitialScreen).Count() + 1;
        }

        private int GetCurrentWorkingScreenIndex()
        {
            var screens = GetScreensAsList();
            return _myScreenManager.WorkingScreen == null
                ? 0
                : screens.TakeWhile(screen => screen != _myScreenManager.WorkingScreen).Count() + 1;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            var screens = GetScreensAsOptions();

            //EditorGUILayout.PropertyField(_initialScreen, new GUIContent("Initial"));
            EditorGUILayout.PropertyField(_useAnimator, new GUIContent("Use Animator"));

            if (_myScreenManager.GetComponent<Screen>() != null) //show the property only if it's a nested screen manager
                EditorGUILayout.PropertyField(_resetOnClose, new GUIContent("Reset On Close"));

            if (_myScreenManager.InitialScreen == null)
            {
                GUI.color = Color.red;
                EditorGUILayout.HelpBox("No initial screen is set yet", MessageType.Warning, true);
                GUI.color = Color.white;
            }

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Initial Screen");
            var currentInitialScreenIndex = GetCurrentInitialScreenIndex();
            _initialScreenIndex = currentInitialScreenIndex;
            _initialScreenIndex = EditorGUILayout.Popup(_initialScreenIndex, screens);
            if (_initialScreenIndex != currentInitialScreenIndex)
                ChangeInitialScreent(_initialScreenIndex);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Working Screen");
            var currentWorkingScreenIndex = GetCurrentWorkingScreenIndex();
            _workingScreenIndex = currentWorkingScreenIndex;
            _workingScreenIndex = EditorGUILayout.Popup(_workingScreenIndex, screens);
            if (_workingScreenIndex != currentWorkingScreenIndex)
                ChangeWorkingScreen(_workingScreenIndex);
            EditorGUILayout.EndHorizontal();

            serializedObject.ApplyModifiedProperties(); 
        }

        private void ChangeWorkingScreen(int index)
        {
            if (EditorApplication.isPlaying) return;

            var screens = GetScreensAsList();
            if (index == 0)
                _myScreenManager.WorkingScreen = null;

            index--;
            Screen newWorkingScreen = null;
            if (index >= 0)
            {
                newWorkingScreen = screens[index];
                newWorkingScreen.gameObject.SetActive(true);
            }

            foreach (var screen in screens)
            {
                if (newWorkingScreen != screen)
                    screen.gameObject.SetActive(false);
            }

            _myScreenManager.WorkingScreen = newWorkingScreen;
        }

        private void ChangeInitialScreent(int index)
        {
            if (index == 0)
            {
                _myScreenManager.InitialScreen = null;
                return;
            }
            index--;
            var screens = GetScreensAsList();
            var newScreen = screens[index];
            _myScreenManager.InitialScreen = newScreen;
        }

        private string[] GetScreensAsOptions()
        {
            var screens = GetScreensAsList();
            var options = new List<string> { "NONE" };
            options.AddRange(screens.Select(screen => screen.transform.name));
            return options.ToArray();
        }

        private List<Screen> GetScreensAsList()
        {
            var screens = new List<Screen>();
            var canvasGameObject = _myScreenManager.gameObject;
            foreach (Transform child in canvasGameObject.transform)
            {
                var screenManager = child.GetComponent<Screen>();
                if (screenManager != null)
                    screens.Add(screenManager);
            }
            return screens;
        }
    }
}
